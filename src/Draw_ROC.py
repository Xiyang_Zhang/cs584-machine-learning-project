import matplotlib.pyplot as plt
import sklearn.metrics as ms
from sklearn.datasets import make_blobs
from sklearn.linear_model import LogisticRegression


class Draw_ROC():
    # 配置全局参数
    plt.rcParams['font.sans-serif'] = ['Simhei']  # 设置中文字体为黑体
    plt.rcParams['axes.unicode_minus'] = False  # 显示负号
    plt.style.use("seaborn-whitegrid")  # 设置画图风格

    def __init__(self, figsize=(7.5, 6)):
        self.figsize = figsize      # 统计学默认图形比例 1.25:1

    def ROC(self,y,prob_1):
        FPR, recall, thresholds = ms.roc_curve(y, prob_1, pos_label=1)
        thresholds = thresholds[::-1]
        area = ms.roc_auc_score(y, prob_1)
        maxindex = (recall - FPR).tolist().index(max(recall - FPR))
        print('The best threshold:{}'.format(thresholds[maxindex]))
        print('The best recall:{}'.format(recall[maxindex]))
        plt.figure(figsize=self.figsize)
        plt.plot(FPR, recall, color='red',label='ROC curve (area = %0.2f)' % area)
        plt.plot([0, 1], [0, 1], color='black', linestyle='--')
        plt.scatter(FPR[maxindex], recall[maxindex], c="black", s=30)
        plt.xlim([-0.05, 1.05])
        plt.ylim([-0.05, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('Recall')
        plt.grid(b=False)
        plt.title('Receiver operating characteristic example')
        plt.legend(loc="lower right")
        plt.show()

def test():
    class_1_ = 300
    class_2_ = 29
    centers_ = [[0.0, 0.0], [1, 1]]
    clusters_std = [1, 0.5]
    X_, y_ = make_blobs(n_samples=[class_1_, class_2_],centers=centers_,
                        cluster_std=clusters_std,random_state=0, shuffle=False)
    model = LogisticRegression()
    model.fit(X_,y_)
    prob = model.predict_proba(X_)
    roc = Draw_ROC()
    roc.ROC(y_,prob[:,1])

if __name__ == '__main__':
    test()